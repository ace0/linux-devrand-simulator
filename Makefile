CC=gcc
CFLAGS=-Isrc

SRC_DIR=src
BIN_DIR=bin
TEST_DIR=tests
LIBS=-lcheck

SOURCES = $(wildcard $(SRC_DIR)/*.c)
HEADERS = $(wildcard $(SRC_DIR)/*.h)
OBJECTS = $(SOURCES:.c=.o)

check: $(TEST_DIR)/test-main.o $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) -Itests $(LIBS)
	@./check

$(BIN_DIR)/%.o: $(SRC_DIR)/%.c $(SOURCES) $(HEADERS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(TEST_DIR)/%.o: $(TEST_DIR)/%.c $(wildcard $(TEST_DIR)/*.h)
	$(CC) -c -o $@ $< $(CFLAGS)
	
.PHONY: clean check

clean:
	rm -f $(BIN_DIR)/* $(TEST_DIR)/*.o *~