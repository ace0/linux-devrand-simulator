/*
 * myio.h
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#ifndef MYIO_H_
#define MYIO_H_

#include <stdbool.h>

// Empty loop
#define EMPTY  do { } while (0)

// Turn debugging output on/off
#ifndef DEBUG_OUTPUT
# define DEBUG_OUTPUT true
#endif

#if DEBUG_OUTPUT

// Print debugging statements.
# define printd(message, args...) printf( "DEBUG: " message "\n", ## args)

// Print a particular variable's name and value using the specified formatter.
# define printv(var, format)  printd( #var": " format, var)

// Print an integer variable's name and value.
# define printi(var)  printv(var, "%d")

// Print an unsigned long, name and value
#define printl(var)  printv(var, "%lu")

// Print a long in hexadecimal.
#define printx(var)  printv(var, "%lX")

// Print a buffer in hexadecimal format.
#define printb(var, size)  printd( #var": %s", hex_from_buffer(var, size) );

// Print a pointer.
#define printp(var)  printv(var, "%p");

// Disable debugging output
#else
# define printd(message, args...) EMPTY
# define printv(var, fmt)         EMPTY
# define printi(var)              EMPTY
# define printl(var)              EMPTY
# define printx(var)              EMPTY
# define printb(var)              EMPTY
# define printp(var)              EMPTY
#endif

const char* hex_from_buffer(const void* buffer, unsigned int size);

#endif /* MYIO_H_ */
