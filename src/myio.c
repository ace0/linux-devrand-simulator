/**
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#include "kernel.h"
#include "myio.h"
#include <string.h>
#include <stdio.h>

#define TEXT_SIZE 2048

// Convert an arbitrary block of memory to a hexadecimal string.
const char* hex_from_buffer(const void* buffer, unsigned int size)
{
	static char text[TEXT_SIZE];
	memset(text, 0, TEXT_SIZE);

	// Formatting an empty buffer is easy!
	if (buffer == NULL || size <= 0) { return ""; }

	// The number of bytes is the minimum of:
	// -Number of bytes in the buffer
	// -Half the output buffer (each byte requires 2-hex characters)
	//  minus a single byte for a string terminator (T2000)
	size = min_t(int, TEXT_SIZE/2 -1, size);

	// Print the buffer one byte at a time.
	for (int i=0; i < size; ++i)
	{
		int rv = snprintf(text+2*i, 3, "%02X", ((unsigned char*)buffer)[i]);
		if (rv < 0) { return "ERROR"; }
	}
	return text;
}
