/*
 * kernel.h
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 *
 * Contains relevant definitions and values taken from linux kernel source and header files.
 */
#ifndef KERNEL_H_
#define KERNEL_H_

#include <stdint.h>

// From: .config
#define CONFIG_X86_64
#define CONFIG_X86_CMOV

// typeof operator is broken when C99 mode is turned on.
#define typeof __typeof__

// From: linux/params.h, .config
#define HZ  250

// Kernel types
typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef uint64_t cycles_t;
typedef long ssize_t;
//typedef long size_t;

// From: linux/typecheck.h
#define typecheck(type,x) \
({	type __dummy; \
	typeof(x) __dummy2; \
	(void)(&__dummy == &__dummy2); \
	1; \
})

// From: linux/jiffies.h
#define time_after(a,b)		\
	(typecheck(unsigned long, a) && \
	 typecheck(unsigned long, b) && \
	 ((long)(b) - (long)(a) < 0))

// From: asm/bitops.h
static inline u32 rol32(u32 word, unsigned int shift)
{
	return (word << shift) | (word >> (32 - shift));
}

static inline u32 ror32(u32 word, unsigned int shift)
{
	return (word >> shift) | (word << (32 - shift));
}

/* GCC complains on OSX of a duplicate definition from 
   /usr/include/strings.h
   Commenting it out for now.  -ACE
static inline int fls(int x)
{
	int r;

#ifdef CONFIG_X86_64
	/ *
	 * AMD64 says BSRL won't clobber the dest reg if x==0; Intel64 says the
	 * dest reg is undefined if x==0, but their CPU architect says its
	 * value is written to set it to the same as before, except that the
	 * top 32 bits will be cleared.
	 *
	 * We cannot do this on 32 bits because at the very least some
	 * 486 CPUs did not behave this way.
	 * /
	long tmp = -1;
	__asm("bsrl %1,%0"
	    : "=r" (r)
	    : "rm" (x), "0" (tmp));
#elif defined(CONFIG_X86_CMOV)
	asm("bsrl %1,%0\n\t"
	    "cmovzl %2,%0"
	    : "=&r" (r) : "rm" (x), "rm" (-1));
#else
	asm("bsrl %1,%0\n\t"
	    "jnz 1f\n\t"
	    "movl $-1,%0\n"
	    "1:" : "=r" (r) : "rm" (x));
#endif
	return r + 1;
}
*/

// From: linux/unaligned/be_byteshift.h
static inline u32 __get_unaligned_be32(const u8 *p)
{
	return p[0] << 24 | p[1] << 16 | p[2] << 8 | p[3];
}

static inline u32 get_unaligned_be32(const void *p)
{
	return __get_unaligned_be32((const u8 *)p);
}

// From: linux/kernel.h
#define min(x, y) ({				\
	typeof(x) _min1 = (x);			\
	typeof(y) _min2 = (y);			\
	(void) (&_min1 == &_min2);		\
	_min1 < _min2 ? _min1 : _min2; })

// From: linux/kernel.h
#define max(x, y) ({				\
	typeof(x) _max1 = (x);			\
	typeof(y) _max2 = (y);			\
	(void) (&_max1 == &_max2);		\
	_max1 > _max2 ? _max1 : _max2; })

// From: linux/kernel.h
#define max_t(type, x, y) ({			\
	type __max1 = (x);			\
	type __max2 = (y);			\
	__max1 > __max2 ? __max1: __max2; })

// From: linux/kernel.h
#define min_t(type, x, y) ({			\
	type __min1 = (x);			\
	type __min2 = (y);			\
	__min1 < __min2 ? __min1: __min2; })

/**
 * cmpxchg C-style pseudocode taken from:
 * http://pdos.csail.mit.edu/6.828/2009/lec/l-rcu.html
 */
static inline int
cmpxchg(int* const addr, const int old, const int new)
{
    const int was = *addr;
    if (was == old)
    	*addr = new;
    return was;
}

// From: linux/cryptohash.h
#define SHA_DIGEST_WORDS 5
#define SHA_MESSAGE_BYTES (512 /*bits*/ / 8)
#define SHA_WORKSPACE_WORDS 16

void sha_init(u32 *buf);
void sha_transform(u32 *digest, const char *data, u32 *W);

#endif /* KERNEL_H_ */
