/**
 * random.c
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#include "random.h"
#include "myio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uthash.h"

/**
 * Global variables.  >:[
 */
static int trickle_thresh = INPUT_POOL_WORDS * 28;
static int random_read_wakeup_thresh = 64;
static int random_write_wakeup_thresh = 128;

static u32 const twist_table[8] =
{	0x00000000, 0x3b6e20c8, 0x76dc4190, 0x4db26158,
	0xedb88320, 0xd6d6a3e8, 0x9b64c2b0, 0xa00ae278 };

/**
 * Uninitialized entropy stores and entropy pools.
 * Use init_pools() to initialize the pools to their proper
 * starting values.
 */
struct entropy_store input_pool, blocking_pool, nonblocking_pool;
struct fast_pool fast_pool;
u32 input_pool_data[INPUT_POOL_WORDS] = {0};
u32 blocking_pool_data[OUTPUT_POOL_WORDS] = {0};
u32 nonblocking_pool_data[OUTPUT_POOL_WORDS] = {0};

/**
 * Static copies of all the pools in their initial states.
 */
static struct poolinfo poolinfo_table[] = {
	/* x^128 + x^103 + x^76 + x^51 +x^25 + x + 1 -- 105 */
	{ 128,	103,	76,	51,	25,	1 },
	/* x^32 + x^26 + x^20 + x^14 + x^7 + x + 1 -- 15 */
	{ 32,	26,	20,	14,	7,	1 },
};

static const struct entropy_store init_input_pool =
{
	.poolinfo = &poolinfo_table[0],
	.name = "input",
	.pull = NULL,
	.limit = 1,
	//.lock = __SPIN_LOCK_UNLOCKED(&input_pool.lock),
	.pool = input_pool_data
};

static const struct entropy_store init_blocking_pool =
{
	.poolinfo = &poolinfo_table[1],
	.name = "blocking",
	.limit = 1,
	.pull = &input_pool,
	//.lock = __SPIN_LOCK_UNLOCKED(&blocking_pool.lock),
	.pool = blocking_pool_data
};

static const struct entropy_store init_nonblocking_pool =
{
	.poolinfo = &poolinfo_table[1],
	.limit = 0,
	.name = "nonblocking",
	.pull = &input_pool,
	//.lock = __SPIN_LOCK_UNLOCKED(&nonblocking_pool.lock),
	.pool = nonblocking_pool_data
};

static const struct fast_pool init_fast_pool;

// Initialize the entropy pools to their starting values.
void init_pools()
{
	// Copy in default structure values.
	memcpy(&fast_pool,        &init_fast_pool,        sizeof(struct fast_pool));
	memcpy(&input_pool,       &init_input_pool,       sizeof(struct entropy_store));
	memcpy(&blocking_pool,    &init_blocking_pool,    sizeof(struct entropy_store));
	memcpy(&nonblocking_pool, &init_nonblocking_pool, sizeof(struct entropy_store));

	// Zero out entropy data.
	memset(fast_pool.pool,        0, FASTPOOL_BYTES);
	memset(input_pool_data,       0, INPUT_POOL_BYTES);
	memset(blocking_pool_data,    0, OUTPUT_POOL_BYTES);
	memset(nonblocking_pool_data, 0, OUTPUT_POOL_BYTES);
}

/**
 * Creates a new timer_rand_state and initializes it with the specified hash key.
 */
struct timer_rand_state* new_rand_state(int key)
{
	// Make a new struct.
	struct timer_rand_state *s = malloc(sizeof(struct timer_rand_state));
	if (s == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for new timer_rand_state struct.");
		return NULL;
	}

	// Initialize the struct.
	memset(s, 0, sizeof(struct timer_rand_state));
	s->hash_key = key;
	return s;
}

/**
 * Retrieve the timer_rand_state identified by a unique key.
 * If this is the first time this key has been seen, a new struct will
 * be created and stored.
 */
struct timer_rand_state* get_rand_state(int key)
{
	// Our hash table.
	static struct timer_rand_state* table = NULL;
	struct timer_rand_state *s = NULL;

	// Check the table for this key.
	HASH_FIND_INT(table, &key, s);

	// If the key wasn't found...
	if (s == NULL)
	{
		// Create a new one and put it into the table.
		s = new_rand_state(key);
		HASH_ADD_INT(table, hash_key, s);
	}

	// Send it back.
	return s;
}

/*
 * Credit (or debit) the entropy store with n bits of entropy
 */
static void credit_entropy_bits(struct entropy_store *r, int nbits)
{
	int entropy_count, orig;

	if (!nbits)
		return;

retry:
	entropy_count = orig = r->entropy_count;
	entropy_count += nbits;

	if (entropy_count < 0)
		entropy_count = 0;
	else if (entropy_count > r->poolinfo->POOLBITS)
		entropy_count = r->poolinfo->POOLBITS;

	if (cmpxchg(&r->entropy_count, orig, entropy_count) != orig)
		goto retry;

	if (!r->initialized && nbits > 0)
	{
		r->entropy_total += nbits;
		if (r->entropy_total > 128)
			r->initialized = 1;
	}
}

// Transfers bits from a primary pool to a secondary pool.
static void xfer_secondary_pool(struct entropy_store *r, size_t nbytes)
{
	u32	tmp[OUTPUT_POOL_WORDS];

	if (r->pull && r->entropy_count < nbytes * 8 &&
	    r->entropy_count < r->poolinfo->POOLBITS)
	{
		/* If we're limited, always leave two wakeup worth's BITS */
		int rsvd = r->limit ? 0 : random_read_wakeup_thresh/4;
		int bytes = nbytes;

		/* pull at least as many as BYTES as wakeup BITS */
		bytes = max_t(int, bytes, random_read_wakeup_thresh / 8);
		/* but never more than the buffer size */
		bytes = min_t(int, bytes, sizeof(tmp));

		bytes = extract_entropy(r->pull, tmp, bytes, random_read_wakeup_thresh / 8, rsvd);
		mix_pool_bytes(r, tmp, bytes, NULL);

		credit_entropy_bits(r, bytes*8);
	}
}

// Yet another function for pulling data from an entropy pool.
static size_t account(struct entropy_store *r, size_t nbytes, int min,
		      int reserved)
{
	/* Can we pull enough? */
	if (r->entropy_count / 8 < min + reserved) {
		nbytes = 0;
	} else {
		/* If limited, never pull more than available */
		if (r->limit && nbytes + reserved >= r->entropy_count / 8)
			nbytes = r->entropy_count/8 - reserved;

		if (r->entropy_count / 8 >= nbytes + reserved)
			r->entropy_count -= nbytes*8;
		else
			r->entropy_count = reserved;
	}

	return nbytes;
}

// Pulls data from an entropy pool for a userland process.
static ssize_t extract_entropy_user(struct entropy_store *r, void *buf,
		size_t nbytes)
{
	ssize_t ret = 0, i;
	u8 tmp[EXTRACT_SIZE];

	xfer_secondary_pool(r, nbytes);
	nbytes = account(r, nbytes, 0, 0);

	while (nbytes)
	{
		extract_buf(r, tmp);
		i = min_t(int, nbytes, EXTRACT_SIZE);
		memcpy(buf, tmp, i);

		nbytes -= i;
		buf += i;
		ret += i;
	}

	/* Wipe data just returned from memory */
	memset(tmp, 0, sizeof(tmp));
	return ret;
}

// Initializes an entropy pool with some seed data
void init_std_data(struct entropy_store *r, void* utsname,
		int size, u64 now)
{
	r->entropy_count = 0;
	r->entropy_total = 0;
	mix_pool_bytes(r, &now, sizeof(now), NULL);
	mix_pool_bytes(r, utsname, size, NULL);
}

// Retrieves random bytes for an in-kernel caller.
void get_random_bytes(void *buf, int nbytes)
{
	extract_entropy(&nonblocking_pool, buf, nbytes, 0, 0);
}

// Pulls data from an entropy pool.
ssize_t extract_entropy(struct entropy_store *r,
			void *buf, size_t nbytes, int min, int reserved)
{
	ssize_t ret = 0, i;
	u8 tmp[EXTRACT_SIZE];

	xfer_secondary_pool(r, nbytes);
	nbytes = account(r, nbytes, min, reserved);

	while (nbytes) {
		extract_buf(r, tmp);

		i = min_t(int, nbytes, EXTRACT_SIZE);
		memcpy(buf, tmp, i);
		nbytes -= i;
		buf += i;
		ret += i;
	}

	/* Wipe data just returned from memory */
	memset(tmp, 0, sizeof(tmp));
	return ret;
}

// Pulls data from an entropy pool.  In the process, it mixes a hashed feedback
// back into the pool.
void extract_buf(struct entropy_store *r, u8 *out)
{
	int i;
	union {
		u32 w[5];
		unsigned long l[LONGS(EXTRACT_SIZE)];
	} hash;
	u32 workspace[SHA_WORKSPACE_WORDS];
	u8 extract[64];

	/* Generate a hash across the pool, 16 words (512 bits) at a time */
	sha_init(hash.w);

	for (i = 0; i < r->poolinfo->poolwords; i += 16)
		sha_transform(hash.w, (u8 *)(r->pool + i), workspace);

	/*
	 * We mix the hash back into the pool to prevent backtracking
	 * attacks (where the attacker knows the state of the pool
	 * plus the current outputs, and attempts to find previous
	 * ouputs), unless the hash function can be inverted. By
	 * mixing at least a SHA1 worth of hash data back, we make
	 * brute-forcing the feedback as hard as brute-forcing the
	 * hash.
	 */
	mix_pool_bytes(r, hash.w, sizeof(hash.w), extract);

	/*
	 * To avoid duplicates, we atomically extract a portion of the
	 * pool while mixing, and hash one final time.
	 */
	sha_transform(hash.w, extract, workspace);
	memset(extract, 0, sizeof(extract));
	memset(workspace, 0, sizeof(workspace));

	/*
	 * In case the hash function has some recognizable output
	 * pattern, we fold it in half. Thus, we always feed back
	 * twice as much data as we output.
	 */
	hash.w[0] ^= hash.w[3];
	hash.w[1] ^= hash.w[4];
	hash.w[2] ^= rol32(hash.w[2], 16);

	memcpy(out, &hash, EXTRACT_SIZE);
	memset(&hash, 0, sizeof(hash));
}

// Mix input into the the fast (interrupt) pool.
void fast_mix(struct fast_pool *f, const void *in, int nbytes)
{
	const char	*bytes = in;
	u32	w;
	unsigned	i = f->count;
	unsigned	input_rotate = f->rotate;

	while (nbytes--)
	{
		w = rol32(*bytes++, input_rotate & 31) ^ f->pool[i & 3] ^
			f->pool[(i + 1) & 3];
		f->pool[i & 3] = (w >> 3) ^ twist_table[w & 7];
		input_rotate += (i++ & 3) ? 7 : 14;
	}
	f->count = i;
	f->rotate = input_rotate;
}

// Mix input into an entropy pool
void mix_pool_bytes(struct entropy_store *r, const void *in,
			     int nbytes, u8 out[64])
{
	unsigned long i, j, tap1, tap2, tap3, tap4, tap5;
	int input_rotate;
	int wordmask = r->poolinfo->poolwords - 1;
	const char *bytes = in;
	u32 w;

	tap1 = r->poolinfo->tap1;
	tap2 = r->poolinfo->tap2;
	tap3 = r->poolinfo->tap3;
	tap4 = r->poolinfo->tap4;
	tap5 = r->poolinfo->tap5;

	input_rotate = r->input_rotate;
	i = r->add_ptr;

	/* mix one byte at a time to simplify size handling and churn faster */
	while (nbytes--) {
		w = rol32(*bytes++, input_rotate & 31);
		i = (i - 1) & wordmask;

		/* XOR in the various taps */
		w ^= r->pool[i];
		w ^= r->pool[(i + tap1) & wordmask];
		w ^= r->pool[(i + tap2) & wordmask];
		w ^= r->pool[(i + tap3) & wordmask];
		w ^= r->pool[(i + tap4) & wordmask];
		w ^= r->pool[(i + tap5) & wordmask];

		/* Mix the result back in with a twist */
		r->pool[i] = (w >> 3) ^ twist_table[w & 7];

		/*
		 * Normally, we add 7 bits of rotation to the pool.
		 * At the beginning of the pool, add an extra 7 bits
		 * rotation, so that successive passes spread the
		 * input bits across the pool evenly.
		 */
		input_rotate += i ? 7 : 14;
	}

	r->input_rotate = input_rotate;
	r->add_ptr = i;

	if (out)
	{
		for (j = 0; j < 16; j++)
			((u32 *)out)[j] = r->pool[(i - j) & wordmask];
	}
}

// Adds device input to input and non-blocking pools
void add_device_randomness(const void *buf, unsigned int size,
		unsigned long jiffies, unsigned long cycles)
{
	unsigned long time = cycles ^ jiffies;
	mix_pool_bytes(&input_pool, buf, size, NULL);
	mix_pool_bytes(&input_pool, &time, sizeof(time), NULL);
	mix_pool_bytes(&nonblocking_pool, buf, size, NULL);
	mix_pool_bytes(&nonblocking_pool, &time, sizeof(time), NULL);
}

// Adds timing-related input to input pool.
void add_timer_randomness(unsigned num, unsigned long jiffies,
		unsigned long cycles)
{
	static int trickle_count = 0;
	struct {
		long jiffies;
		unsigned cycles;
		unsigned num;
	} sample;
	long delta, delta2, delta3;

	/* if over the trickle threshold, use only 1 in 4096 samples */
	if ( input_pool.entropy_count > trickle_thresh &&
       ( (trickle_count++) & 0xfff) )
		return;

	sample.jiffies = jiffies;
	sample.cycles = cycles;
	sample.num = num;

	mix_pool_bytes(&input_pool, &sample, sizeof(sample), NULL);

	// Get the state for this num value.
	struct timer_rand_state *state = get_rand_state(num);

	/*
	 * Calculate number of bits of randomness we probably added.
	 * We take into account the first, second and third-order deltas
	 * in order to make our estimate.
	 */

	if (!state->dont_count_entropy) {
		delta = sample.jiffies - state->last_time;
		state->last_time = sample.jiffies;

		delta2 = delta - state->last_delta;
		state->last_delta = delta;

		delta3 = delta2 - state->last_delta2;
		state->last_delta2 = delta2;

		if (delta < 0)
			delta = -delta;
		if (delta2 < 0)
			delta2 = -delta2;
		if (delta3 < 0)
			delta3 = -delta3;
		if (delta > delta2)
			delta = delta2;
		if (delta > delta3)
			delta = delta3;

		credit_entropy_bits(&input_pool,
				    min_t(int, fls(delta>>1), 11));
	}
}

// Adds interrupt input into the fast-pool or non-blocking pool depending
// on internal state values.
void add_interrupt_randomness(int irq, int irq_flags,
		unsigned long jiffies, unsigned long cycles,
		unsigned long rip)
{
	struct entropy_store	*r;
	struct fast_pool	*fp = &fast_pool;
	unsigned long	now = jiffies;
	u32	input[4];
	u64 ip = rip;

	input[0] = cycles ^ jiffies;
	input[1] = irq;
	input[2] = ip;
	input[3] = ip >> 32;

	fast_mix(fp, input, sizeof(input));

	if ((fp->count & 1023) &&
	    !time_after(now, fp->last + HZ))
		return;

	fp->last = now;

	r = nonblocking_pool.initialized ? &input_pool : &nonblocking_pool;
	mix_pool_bytes(r, &fp->pool, sizeof(fp->pool), NULL);

	credit_entropy_bits(r, 1);
}

// Extracts entropy from the blocking pool whenever a read from /dev/random
// occurs.
ssize_t random_read(char* buffer, size_t nbytes)
{
	ssize_t n, retval = 0, count = 0;
	if (nbytes == 0) return 0;

	while (nbytes > 0)
	{
		n = nbytes;
		if (n > SEC_XFER_SIZE)
			n = SEC_XFER_SIZE;

		n = extract_entropy_user(&blocking_pool, buffer, n);

		if (n == 0)
		{
			fprintf(stderr, "ERROR: not enough entropy to satisfy read request from /dev/random.\n");
			return -EXIT_FAILURE;
		}
//			if (file->f_flags & O_NONBLOCK)
//			{
//				retval = -EAGAIN;
//				break;
//			}

			// Disabled blocking features.
//			wait_event_interruptible(random_read_wait,
//				input_pool.entropy_count >=
//						 random_read_wakeup_thresh);

//			if (signal_pending(current))
//			{
//				retval = -ERESTARTSYS;
//				break;
//			}
//			continue;
//		}

		if (n < 0)
		{
			retval = n;
			break;
		}
		count += n;
		buffer += n;
		nbytes -= n;
		break;		/* This break makes the device work */
							/* like a named pipe */
	}
	return (count ? count : retval);
}

// Extracts entropy from the nonblocking pool whenever a read from /dev/urandom
// occurs.
ssize_t urandom_read(char* buffer, size_t nbytes)
{
	return extract_entropy_user(&nonblocking_pool, buffer, nbytes);
}

