/**
 * random.h
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#ifndef RANDOM_H_
#define RANDOM_H_

#include <stdlib.h>
#include "kernel.h"
#include "uthash.h"

// Constants/macros from random.c
#define POOLBITS	         poolwords*32
#define POOLBYTES	         poolwords*4
#define INPUT_POOL_WORDS   128
#define OUTPUT_POOL_WORDS   32
#define SEC_XFER_SIZE      512
#define EXTRACT_SIZE        10

#define LONGS(x) (((x) + sizeof(unsigned long) - 1)/sizeof(unsigned long))

// Constants that we've defined
#define INPUT_POOL_BYTES      (INPUT_POOL_WORDS*4)
#define OUTPUT_POOL_BYTES     (OUTPUT_POOL_WORDS*4)
#define FASTPOOL_BYTES        16
#define FASTPOOL_WORDS        (FASTPOOL_BYTES/4)
#define MIX_POOL_OUTPUT_BYTES 64
#define SHA_WORKSPACE_BYTES 	(SHA_WORKSPACE_WORDS*4)
#define INITIAL_HASH_BYTES 		(5*4)

/**
 * Structure definitions
 */

struct poolinfo
{
	int poolwords;
	int tap1, tap2, tap3, tap4, tap5;
};

struct entropy_store
{
	/* read-only data: */
	struct poolinfo *poolinfo;
	u32 *pool;
	const char *name;
	struct entropy_store *pull;
	int limit;

	/* read-write data: */
	//spinlock_t lock;
	unsigned add_ptr;
	unsigned input_rotate;
	int entropy_count;
	int entropy_total;
	unsigned int initialized:1;
	u8 last_data[EXTRACT_SIZE];
};

struct fast_pool
{
	u32						 pool[FASTPOOL_WORDS];
	unsigned long	 last;
	unsigned short count;
	unsigned char	 rotate;
	unsigned char	 last_timer_intr;
};

struct timer_rand_state
{
	// Makes this structure compatible with the UT hash table.
	int hash_key;
	UT_hash_handle hh;

	cycles_t last_time;
	long last_delta, last_delta2;
	unsigned dont_count_entropy:1;
};

/**
 * Global variables - >:-\  and :(     (evil and sad)
 */
extern struct entropy_store blocking_pool, nonblocking_pool, input_pool;
extern struct fast_pool fast_pool;

/**
 * Function prototypes
 */
void init_pools();
void init_std_data(struct entropy_store *r, void* utsname,
		int size, u64 now);
void add_device_randomness(const void *buf, unsigned int size,
		unsigned long jiffies, unsigned long cycles);
void add_timer_randomness(unsigned num, unsigned long jiffies,
		unsigned long cycles);
void add_interrupt_randomness(int irq, int irq_flags,
		unsigned long jiffies, unsigned long cycles,
		unsigned long ip);
void fast_mix(struct fast_pool *f, const void *in, int nbytes);
void mix_pool_bytes(struct entropy_store *r, const void *in,
		int nbytes, u8 out[64]);
void extract_buf(struct entropy_store *r, u8 *out);
void get_random_bytes(void *buf, int nbytes);
ssize_t extract_entropy(struct entropy_store *r,
			void *buf, size_t nbytes, int min, int reserved);
ssize_t random_read(char* buffer, size_t nbytes);
ssize_t urandom_read(char* buffer, size_t nbytes);
struct timer_rand_state* get_rand_state(int key);
struct timer_rand_state* new_rand_state(int key);


#endif /* RANDOM_H_ */

