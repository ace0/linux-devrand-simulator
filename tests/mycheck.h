/**
 * Copyright (C) 2013  Adam C Everspaugh
 * All rights reserved.
 */
#ifndef MYCHECK_H
#define MYCHECK_H

#include <check.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

void register_test(TCase*);

/**
 * Shortcuts for the verbose ck_assert_type_op macros.
 * Instead, we use the following convention: operation/type
 * with no separator.  For instance:
 * eqs:  equal, string
 * neqi: not-equal, integer
 * lef:  less-than-or-equal, float
 */
#define check(C)  ck_assert(C)

// s = string
#define eqs(X, Y)  ck_assert_str_eq(X, Y)
#define nes(X, Y)  ck_assert_str_neq(X, Y)

// ui = unsigned int
#define equi(X, Y)  ck_assert_uint_eq(X, Y)
#define neui(X, Y)  ck_assert_uint_neq(X, Y)
#define ltui(X, Y)  ck_assert_uint_lt(X, Y)
#define leui(X, Y)  ck_assert_uint_le(X, Y)
#define gtui(X, Y)  ck_assert_uint_gt(X, Y)
#define geui(X, Y)  ck_assert_uint_ge(X, Y)

// i = int
#define eqi(X, Y)  ck_assert_int_eq(X, Y)
#define nei(X, Y)  ck_assert_int_neq(X, Y)
#define lti(X, Y)  ck_assert_int_lt(X, Y)
#define lei(X, Y)  ck_assert_int_le(X, Y)
#define gti(X, Y)  ck_assert_int_gt(X, Y)
#define gei(X, Y)  ck_assert_int_ge(X, Y)

// p = pointer
#define eqp(X, Y)  ck_assert_ptr_eq(X, Y)
#define nep(X, Y)  ck_assert_ptr_neq(X, Y)

// Guess the type based on the size of the variable.
#define eq(X, Y) if (sizeof(X) == sizeof(int)) { ck_assert_int_eq(X, Y); } \
						else if (sizeof(X) == sizeof(unsigned int)) { ck_assert_uint_eq(X, Y); } \
						else if (sizeof(X) == sizeof(char*)) { ck_assert_str_eq(X, Y); } \
						else ck_assert_msg(false, "Unknown variable type.  Must be one of int, uint, or char*");

// Verify the contents of an array
#define equi_array(expected, actual, size) \
	for (int i=0; i<size; ++i) \
			equi((expected)[i], (actual)[i]);

/**
 * Register the names of our test with the primary test case
 */
#define RUN void register_test(TCase* tc)

/**
 * Register an individual test by name
 */
#define TEST(name) tcase_add_test(tc, name)

/**
 * Quick and dirty test runner.  Runs any tests that have been registered
 * with the RUN and TEST macros defined above.
 */
int main(void)
{
  int number_failed;

  // Setup a single suite and test case.
  Suite *suite = suite_create("All tests");
  TCase *tc_core = tcase_create("Core");
  suite_add_tcase(suite, tc_core);

  // Register the names of all our tests.
  register_test(tc_core);

  // Setup a suite runner and run our tests.
  SRunner *runner = srunner_create(suite);
  srunner_run_all(runner, CK_NORMAL);
  number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  // Report status.
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

#endif // MYCHECK_H
