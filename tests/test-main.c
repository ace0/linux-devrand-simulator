/**
 * Copyright (C) 2013  Adam C Everspaugh
 * All rights reserved.
 */
#include "mycheck.h"
#include "test.h"
#include "fast-mix-test.h"
#include "mix-pool-test.h"
#include "extract-buffer-test.h"
#include "add-interrupt-test.h"
#include "add-device-test.h"
#include "full-trace-test.h"
#include "rand-state-test.h"
#include "reset-test-short.h"
#include "reset-test-long.h"
#include "reset-test-temporary.h"

// Initialize an entropy pool to a known state.
void initialize_pool(struct entropy_store* es,
		const u8* pool,
		unsigned size,
		unsigned input_rotate,
		unsigned add_ptr)
{
	// Sanity check the poolsize.
	equi(es->poolinfo->POOLBYTES, size);

	// Setup the pool with the values provided.
	es->input_rotate = input_rotate;
	es->add_ptr = add_ptr;
	memcpy(es->pool, pool, size);
}

// Tests all fields of an entropy store against an expected value.
void test_entropy_store(const struct entropy_store* expected,
		const struct entropy_store* actual)
{
	// Verify specific fields.
	equi(expected->add_ptr, actual->add_ptr);
	equi(expected->input_rotate, actual->input_rotate);
	equi(expected->entropy_count, actual->entropy_count);
	equi(expected->entropy_total, actual->entropy_total);
	equi(expected->initialized, actual->initialized);

	// Verify the contents of data arrays.
	equi_array(expected->pool, actual->pool, actual->poolinfo->poolwords);
	equi_array(expected->last_data, actual->last_data, EXTRACT_SIZE);
}

// Tests all fields of a fast pool against an expected value.
void test_fast_pool(const struct fast_pool* expected,
		const struct fast_pool* actual)
{
	// Test non-pool fields.
	equi(expected->count, actual->count);
	equi(expected->last, actual->last);
	equi(expected->last_timer_intr, actual->last_timer_intr);
	equi(expected->rotate, actual->rotate);

	// Test the pool contents word-by-word.
	equi_array(expected->pool, actual->pool, FASTPOOL_WORDS);
}

// Run each unit test.
RUN
{
	// Rand-state hash table operations.
	TEST(rand_test_new);
	TEST(rand_test_get_new);
	TEST(rand_test_change);
	TEST(rand_test_multiple);

	// Fast-mix
	TEST(test_fast_mix_zero);
	TEST(test_fast_mix_1);
	TEST(test_fast_mix_2);

	// Mix-pool-bytes
	TEST(test_mix_nonblocking);
	TEST(test_mix_blocking);
	TEST(test_mix_input);

	// Other simple tests
	TEST(extract_buffer_1);
	TEST(get_random_bytes_1);

	// Multi-input tests.
	TEST(add_interrupt_short);
	TEST(add_interrupt_looong);
	TEST(add_device_test);
	TEST(full_trace);

	// Reset tests.
	TEST(reset_test_short);
	TEST(reset_test_long);

	// Dynamically generated unit tests.
	TEST(reset_sanity_test);
}
