/*
 * rand-state-test.h
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
#include "random.h"
#include "mycheck.h"

// Test creating a new timer_rand_state struct.
START_TEST(rand_test_new)
{
	// Generate a new timer state struct.
	struct timer_rand_state* actual;
	actual = new_rand_state(1);

	// Verify the contents of the struct.
	check(actual != 0);
	check(actual->dont_count_entropy == 0);
	check(actual->hash_key == 1);
	check(actual->last_delta == 0);
	check(actual->last_delta2 == 0);
	check(actual->last_time == 0);

	free(actual);
}
END_TEST

// Test getting a new rand_state from the table.
START_TEST(rand_test_get_new)
{
	// Generate a new timer state struct.
	struct timer_rand_state* actual;
	actual = get_rand_state(1);

	// Verify the contents of the struct.
	check(actual != 0);
	check(actual->dont_count_entropy == 0);
	check(actual->hash_key == 1);
	check(actual->last_delta == 0);
	check(actual->last_delta2 == 0);
	check(actual->last_time == 0);
}
END_TEST

// Test changing a value in the table.
START_TEST(rand_test_change)
{
	// Generate a new timer state struct.
	struct timer_rand_state* actual;
	actual = get_rand_state(10);

	// Make some updates.
	actual->dont_count_entropy = 1;
	actual->last_delta = 100;
	actual->last_delta2 = 1212;
	actual->last_time = 0xFACEFACE;

	// Fetch it again.
	actual = 0;
	actual = get_rand_state(10);

	// Verify our fields.
	check(actual != 0);
	check(actual->dont_count_entropy == 1);
	check(actual->hash_key == 10);
	check(actual->last_delta == 100);
	check(actual->last_delta2 == 1212);
	check(actual->last_time ==  0xFACEFACE);
}
END_TEST

// Test our table with multiple states.
START_TEST(rand_test_multiple)
{
	// Generate a new timer state struct.
	struct timer_rand_state* a, *b;
	a = get_rand_state(100);
	b = get_rand_state(200);

	// Make some updates.
	a->last_delta = 150;
	b->last_time = 250;

	// Fetch them again.
	a = 0;
	b = 0;
	a = get_rand_state(100);
	b = get_rand_state(200);

	// Test.
	check(a->hash_key == 100);
	check(a->last_delta == 150);
	check(a->last_time == 0);

	check(b->hash_key == 200);
	check(b->last_delta == 0);
	check(b->last_time == 250);
}
END_TEST

