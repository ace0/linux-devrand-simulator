/*
 * fast-mix-tests.h
 *
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 */
// Setup a fast pool, mix input, and test the resulting structure and pool
void test_fast_mix(const u8* initial_pool,
									 const int initial_count,
									 const int initial_rotate,
									 const u8* input,
									 const u8* expected_pool,
						 	 	 	 const int expected_count,
						 	 	 	 const int expected_rotate)
{
	// Setup the pool to the pre-mix pool state.
	struct fast_pool fp;
	memset(&fp, 0, sizeof(struct fast_pool));
	memcpy(fp.pool, initial_pool, FASTPOOL_BYTES);
	fp.count = initial_count;
	fp.rotate = initial_rotate;

	// Run fast-mix
	fast_mix(&fp, input, FASTPOOL_BYTES);

	// Test the count and rotate values.
	equi(expected_count,  fp.count);
	equi(expected_rotate, fp.rotate);

	// Test contents of the fast pool byte-byte.
  equi_array(expected_pool, (u8*)fp.pool, FASTPOOL_BYTES);
}

// Test fast-pool mixing against known input-output values
// when the pool starts in the all zero (initial) state.
START_TEST(test_fast_mix_zero)
{
	const u8 input[FASTPOOL_BYTES] =   { 0x13, 0x1B, 0xB5, 0xF3,
																			 0x00, 0x00, 0x00, 0x00,
																			 0x8B, 0xD3, 0x03, 0x81,
																			 0xFF, 0xFF, 0xFF, 0xFF };
	const u8 pre_pool[FASTPOOL_BYTES] = { 0 };
	const u8 post_pool[FASTPOOL_BYTES] = { 0xDE, 0x0C, 0x6D, 0xD4,
																				 0x0E, 0x4B, 0xDE, 0xE5,
																				 0xE6, 0xEF, 0x1D, 0xF4,
																				 0x0B, 0xE9, 0x43, 0x62 };
	const int count = 16;  // Expected value
	const int rotate = 140; // Expected value

	// Setup the pool, mix input, and test the result.
	test_fast_mix(pre_pool, 0, 0, input,
			post_pool, count, rotate);
}
END_TEST

// Test fast-pool mixing against known input-output values.
START_TEST(test_fast_mix_1)
{
	const u8 pre_pool[FASTPOOL_BYTES] =
	{ 0xE8, 0x4C, 0xB3, 0x87, 0x5A, 0x4E, 0x63, 0x9B,
		0x1E, 0x08, 0x59, 0x58, 0xD0, 0x94, 0x36, 0x04 	};
	const u8 input[FASTPOOL_BYTES] =
	{ 0x14, 0x5D, 0x23, 0xBC, 0x00, 0x00, 0x00, 0x00,
		0x44, 0x90, 0xD0, 0x81, 0xFF, 0xFF, 0xFF, 0xFF 	};
	const u8 post_pool[FASTPOOL_BYTES] =
	{ 0xEF, 0xC2, 0xC4, 0x21, 0x78, 0x33, 0x47, 0x5F,
		0xDD, 0x69, 0xC5, 0x6A, 0xCC, 0x8C, 0x54, 0x34 };
	const int pre_count = 288;
	const int pre_rotate = 216;
	const int post_count = 304;
	const int post_rotate = 100;

	// Setup the pool, mix input, and test the result.
	test_fast_mix(pre_pool, pre_count, pre_rotate,
			input, post_pool, post_count, post_rotate);
}
END_TEST

// Test fast-pool mixing against known input-output values.
START_TEST(test_fast_mix_2)
{
	const u8 pre_pool[FASTPOOL_BYTES] =
	{	0x0B, 0xCF, 0xD2, 0xA9, 0x7E, 0xCC, 0x86, 0x53,
		0xF8, 0xF5, 0x57, 0x41, 0x4D, 0x2D, 0xD2, 0xB1 };
	const u8 input[FASTPOOL_BYTES] =
	{ 0x5D, 0xD1, 0x57, 0xDE, 0x0F, 0x00, 0x00, 0x00,
		0x3C, 0x2B, 0x01, 0x81, 0xFF, 0xFF, 0xFF, 0xFF 	};
	const u8 post_pool[FASTPOOL_BYTES] =
	{ 0xDD, 0x93, 0xD6, 0x01, 0x50, 0x02, 0x7D, 0x5A,
		0xCD, 0xA3, 0x83, 0x10, 0x06, 0xC8, 0xBB, 0xE1 	};
	const int pre_count = 2064;
	const int pre_rotate = 140;
	const int post_count = 2080;
	const int post_rotate = 24;

	// Setup the pool, mix input, and test the result.
	test_fast_mix(pre_pool, pre_count, pre_rotate,
			input, post_pool, post_count, post_rotate);
}
END_TEST
