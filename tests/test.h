/*
 * test.h
 *
 *  Created on: Jun 10, 2013
 *      Author: ace
 */
#ifndef TEST_H_
#define TEST_H_

#include "random.h"

// Scratch space for storing random bytes retrieved from get_random_bytes
#define MAX_GRB 1024
static u8* rando[MAX_GRB] = {0};

void initialize_pool(struct entropy_store* es,
		const u8* pool,
		unsigned size,
		unsigned input_rotate,
		unsigned add_ptr);
void test_entropy_store(const struct entropy_store* expected,
		const struct entropy_store* actual);
void test_fast_pool(const struct fast_pool* expected,
		const struct fast_pool* actual);

#endif /* TEST_H_ */
